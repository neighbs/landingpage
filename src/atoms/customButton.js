import { Button } from 'antd';

/* eslint-disable react/prop-types */ // TODO: upgrade to latest eslint tooling
export default function CustomButton({ buttonType, onClickFunction, isBlock, children }) {
  return (
    <Button type={buttonType} onClick={onClickFunction} block={isBlock}>
      {children}
    </Button>
  );
}
