/* eslint-disable react/prop-types */
export default function Logo({ width, height }) {
  const style = {
    width,
    height
  };
  return (
    <img src="https://cdn-icons-png.flaticon.com/512/25/25694.png" style={style} alt="neighbs" />
  );
}
