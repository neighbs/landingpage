/* eslint-disable react/prop-types */
import { Form, Input, Button } from 'antd';
import CustomButton from '../atoms/customButton';

function Subscribe({ changeMapVisibility }) {
  return (
    <Form
      name="basic"
      wrapperCol={{ span: 8, offset: 8 }}
      initialValues={{ remember: true }}
      autoComplete="off">
      <Form.Item
        name="Email"
        rules={[
          { required: true, message: 'Το email είναι υποχρεωτικό' },
          { type: 'email', message: 'Λάθος email format' }
        ]}>
        <Input placeholder="Email" />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 8 }}>
        <CustomButton buttonType="link" onClickFunction={changeMapVisibility} isBlock>
          {' '}
          Καταχώρησε την γειτονιά σου{' '}
        </CustomButton>
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 8 }}>
        <Button type="primary" htmlType="submit" block>
          Εγγραφή
        </Button>
      </Form.Item>
    </Form>
  );
}

export default Subscribe;
