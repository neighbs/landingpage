import { Row, Col } from 'antd';
import Logo from '../atoms/logo';
import IntroText from '../atoms/introText';

export default function IntroMessage() {
  return (
    <Row>
      <Col span={8} offset={8}>
        <Col offset={8}>
          <Logo width={100} height={100} />
        </Col>
        <IntroText />
      </Col>
    </Row>
  );
}
