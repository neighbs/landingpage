import Logo from '../atoms/logo';

export default function Brand() {
  return <Logo width={30} height={30} />;
}
