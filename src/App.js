import Home from './pages/Home';
import 'antd/dist/antd.css';
import './index.css';
import './style.css';
import 'leaflet/dist/leaflet.css';

export default function App() {
  return <Home />;
}
