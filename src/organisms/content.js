import React, { useState } from 'react';
import { Layout, Row, Col, Modal } from 'antd';
import IntroMessage from '../molecules/introMessage';
import Subscribe from '../molecules/subscribe';
import NeighbMap from '../molecules/neighbMap';

export default function Content() {
  const [showMap, setshowMap] = useState(false);

  const changeMapVisibility = () => {
    setshowMap((mapVisibility) => !mapVisibility);
  };

  const { Content: ContentAnt } = Layout;

  return (
    <ContentAnt>
      <Row justify="center" align="middle" style={{ height: '100%' }}>
        <Col span={24}>
          <IntroMessage />
          <Subscribe changeMapVisibility={changeMapVisibility} />
        </Col>
        {showMap ? (
          <Modal
            title="Σχεδίασε την γειτονιά σου επιλέγοντας σημεία στον χάρτη "
            centered
            visible={showMap}
            onOk={() => setshowMap(false)}
            onCancel={() => setshowMap(false)}
            width={1000}>
            <NeighbMap />
          </Modal>
        ) : null}
      </Row>
    </ContentAnt>
  );
}
