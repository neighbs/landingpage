import { Layout, Row, Col } from 'antd';
import Brand from '../molecules/brand';

export default function Header() {
  const { Header: HeaderAnt } = Layout;
  return (
    <HeaderAnt>
      <Row justify="space-between">
        <Col>
          <Brand />
        </Col>
        <Col>ΛΙΓΑ ΛΟΓΙΑ ΓΙΑ ΜΑΣ</Col>
      </Row>
    </HeaderAnt>
  );
}
