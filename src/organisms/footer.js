import { Row, Col, Layout } from 'antd';

export default function Footer() {
  const { Footer: FooterAnt } = Layout;
  return (
    <FooterAnt>
      <Row justify="start">
        <Col span={8} offset={8}>
          Footer
        </Col>
      </Row>
    </FooterAnt>
  );
}
