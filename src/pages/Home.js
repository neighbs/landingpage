import { Layout } from 'antd';
import Header from '../organisms/header';
import Footer from '../organisms/footer';
import Content from '../organisms/content';

export default function Home() {
  return (
    <Layout style={{ height: '100vh' }}>
      <Header />
      <Content />
      <Footer />
    </Layout>
  );
}
